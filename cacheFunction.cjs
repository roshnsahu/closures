function cacheFunction(add) {


    if (arguments.length !== 1 || typeof (add) !== 'function') {

        throw new Error('Multiple Arguments')

    }

    let obj = {};

    return function (...args) {

        let key = JSON.stringify(args);

        if (key in obj) {
            return obj[key];
        }

        const result = add(...args);
        obj[key] = result;
        return result;
    }
}


module.exports = cacheFunction;
