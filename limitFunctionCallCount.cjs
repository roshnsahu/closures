function limitFunctionCallCount(cb, n) {
    let count = 0;
    if (arguments.length !== 2 || typeof (cb) !== 'function' || typeof (n) !== 'number') {
        throw new Error('Multiple arguments failed');
    }

    return function (...args) {

        count++;

        if (count <= n) {

            return cb(...args);

        }
        return null;
    }

}

module.exports = limitFunctionCallCount;