let cacheFunction = require('../cacheFunction.cjs');

const add = (a,b) => a + b;

const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1,2)) // Add invoked
console.log(cachedAdd(1,2)); // Cache return
console.log(cachedAdd(2,3)); // Add invoked
console.log(cachedAdd(2,3)); // Cache return
console.log(cachedAdd(2,1)); // Add invoked


try {
    const cachedAdd2 = cacheFunction();
    console.log(cachedAdd2(1, 2));
    
} catch (error) {
    
    console.log('One or more errors found.')
    
}