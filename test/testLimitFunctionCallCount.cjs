const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

function add(a,b)
{
    return a+b;
}

const limitedAddFn = limitFunctionCallCount(add, 2);

console.log(limitedAddFn(1,2)); 
console.log(limitedAddFn(3,4)); 
console.log(limitedAddFn(4,5));


try {
    const limitedAddFn2 = limitFunctionCallCount(add);
    console.log(limitedAddFn2(1, 2));

} catch (e) {
    console.error(e);
}

try {
    const limitedAddFn3 = limitFunctionCallCount();
    console.log(limitedAddFn2(1, 2));

} catch (e) {
    console.error(e);
}
