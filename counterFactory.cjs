// New Code
function counterFactory () {
    let counter = 0;
    
    return {
        increment: () => {
            return ++counter;
        },
        decrement: () => {
            return --counter;
        },
    }
}

module.exports = counterFactory;


// function counterFactory() {
//     let counter = 0;
//     function increment(){
//         return ++counter;
//     }

//     function decrement(){
//         return --counter;
//     }
//     return {'increment':increment(), 'decrement': decrement()}
// }
// console.log(counterFactory().increment());